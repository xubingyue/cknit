cmake_minimum_required(VERSION 3.10)
project(cknit C)

set(CMAKE_C_STANDARD 99)

set(MAIN_SOURCE main.c)

include_directories(
        src/exjson
        src/http
        src/task
        src/timer
        src/events
        src/log
        src/tool
)

aux_source_directory(src/exjson EXJSON_SOURCE)
aux_source_directory(src/http   HTTP_SORUCE)
aux_source_directory(src/task   TASK_SOURCE)
aux_source_directory(src/timer  TIMER_SOURCE)
aux_source_directory(src/events EVENT_SOURCE)
aux_source_directory(src/log    LOG_SOURCE)
aux_source_directory(src/tool   TOOL_SOURCE)

add_executable(
        cknit
        ${MAIN_SOURCE}
        ${EXJSON_SOURCE}
        ${HTTP_SORUCE}
        ${TASK_SOURCE}
        ${TIMER_SOURCE}
        ${EVENT_SOURCE}
        ${LOG_SOURCE}
        ${TOOL_SOURCE}
)

target_link_libraries(cknit pthread)

install(TARGETS cknit RUNTIME DESTINATION /usr/local/bin)
install(FILES conf/cknit.exjson DESTINATION /etc/cknit)
install(FILES conf/task.exjson DESTINATION /etc/cknit)


