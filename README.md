# cknit #

**cknit** 是一款开源高可用定时多任务管理工具，定时精度为秒级别 ( 相比**cron**增加了秒的取值 )，能够确保高效、稳定的处理多任务。
定时精度随任务量的变化如下所示 ( **测试平台OSX** )：

| 任务数量 | 定时精度偏差 |
| :------: | :----------: |
|   1000   |    0.01s     |
|  100000  |      1s      |

# 支持平台 #

目前支持 **Linux**、**mac** 两大平台，mac 平台使用 select 系统调用，Linux平台使用 Posix (timer)，因此 Linux 平台性能比 mac 平台稍高，任务调度精度更佳

# 时间间隔格式

标准格式： 

`* * * * * *`  

| 列   | 含义 | 取值范围 | 特殊值含义     |
| ---- | ---- | -------- | -------------- |
| 1    | 秒   | 0-60     | 正常含义       |
| 2    | 分   | 0-59     | 正常含义       |
| 3    | 时   | 0-23     | 0:表示午夜12点 |
| 4    | 日   | 1-31     | 正常含义       |
| 5    | 月   | 0-11     | 0:表示一月     |
| 6    | 周   | 0-6      | 0:表示周日     |

#### 可选模式：

- `/`   ： 斜线表示每隔多少区间，例如：

  `*/3 * * * * *`

  表示每隔三秒执行一次任务

- `,`  ：逗号表示可选值，例如：

  `*/3 1,2,5,10 * * * *`

  表示在第1、2、5、10分钟每隔3秒执行一次任务

- `-` ：表示区间范围，**区间取值是包含边界值**，例如：

  `* 1-10,15,30-35 * * * *`

  表示在第1到10分钟、15分钟、30到35分钟的每一秒都执行一次任务

上面三种模式可以任意组合

# 设计架构 #

![cknit](cknit.png)

# 安装 #

**cknit** 采用 **cmake** 编译系统，因此需要目标机器安装 **cmake 3.13** 及以上版本

**1、下载源码**

```shell
git clone https://gitee.com/josinli/cknit.git
```

**2、编译**

```shell
mkdir build
cd build
cmake ..
make && make install
cknit
```

# APIs管理 #

安装完成后，访问：

```
http://127.0.0.1:9898
```

响应如下：

```
{
    "message": "Welcome use cknit",
    "code": "ok",
    "version": "1.0",
    "port": 9898,
    "APIs": [
        {
            "name": "Get all monitors tasks",
            "method": "GET",
            "protocol": "HTTP/1.1",
            "url": "http://127.0.0.1:9898/monitors"
        },
        {
            "name": "Add one monitors tasks",
            "method": "POST",
            "protocol": "HTTP/1.1",
            "url": "http://localhost:9898/monitors"
        },
        {
            "name": "Modify one monitors tasks",
            "method": "PUT",
            "protocol": "HTTP/1.1",
            "url": "http://localhost:9898/monitors"
        }
    ]
}
```

### API: 获取当前所有的任务 ###

```
GET http://127.0.0.1:9898/monitors
```

响应回答如下：

```
[
    {
        "command": "php ~/Desktop/index.php",
        "period": "* 1,2,3,10-20 * * * *",
        "id": 1,
        "status": 0
    },
    {
        "command": "php ~/Desktop/index.phpd",
        "period": "* * * * * * */2"
    }
]
```

### API: 在线添加任务 ###

```
POST http://127.0.0.1:9898/monitors
{
	"command": "php ~/Desktop/index.php",
	"period": "* * * * * * */2"
}
```

响应回答如下：

```
{
    "message": "Success",
    "code": "true",
    "operation": "Add task"
}
```

### API: 在线修改已存任务(id是系统自动分配的) ###

```
PUT http://127.0.0.1:9898/monitors
{
	"id": 998,
	"data": {
		"status":0,
		"period": "* * * 11 * */2",
	}
}
```

响应回答如下：

```
{
    "message": "Success",
    "code": "true",
    "operation": "Modify task"
}
```



