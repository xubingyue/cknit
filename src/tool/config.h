/**
 * Copyright @2019 Josin All Rights Reserved.
 * Author: Josin
 * Email : xeapplee@gmail.com
 */

#ifndef LISTS_CONFIG_H
#define LISTS_CONFIG_H

#include <stdio.h>

#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <signal.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#define true  1
#define false 0

#define DEBUG_MODE true

#define EVENT_SIZE      100
#define BUFFER_SIZE     256
#define EVENT_TIMEOUT   120
#define MAX_PROCESS_NUM 256

#define INFO_LOG_NAME  "info_log.log"
#define NOTI_LOG_NAME  "notice_log.log"
#define WARN_LOG_NAME  "warning_log.log"
#define ERRO_LOG_NAME  "error_log.log"

#define memzero(v, l) memset(v, 0, l)
#ifndef e_memfree
    #define e_memfree free
#endif


#endif /* LISTS_CONFIG_H */
